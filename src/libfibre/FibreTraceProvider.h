#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER fibre_tpp

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "libfibre/FibreTraceProvider.h"

#if !defined(_Fibre_Trace_Provider_h_) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _Fibre_Trace_Provider_h_

#include <lttng/tracepoint.h>

TRACEPOINT_EVENT(
  fibre_tpp, // provider name
  fibre_print, // tracepoint name

  /* Input arguments */
  TP_ARGS(
      char*, output_str
  ),

  /* Output event fields */
  TP_FIELDS(
    ctf_string(my_string, output_str)
  )
)

TRACEPOINT_EVENT(
  fibre_tpp, // provider name
  fibre_get_counter, // tracepoint name
  TP_ARGS(
      int, counter,
      char*, output_str
  ),
  TP_FIELDS(
    ctf_integer(int, test_counter, counter)
    ctf_string(my_string, output_str)
  )
)

#endif /*_Fibre_Trace_Provider_h_ */

#include <lttng/tracepoint-event.h>
