/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef SYSCALL_CMP
#define SYSCALL_CMP(call,cmp,expected,errcode) ({\
  int ret ## __COUNTER__ = call;\
  if slowpath(!(ret ## __COUNTER__ cmp expected || ret ## __COUNTER__ == errcode || errno == errcode)) {\
    _SYSCALLabortLock();\
    printf("FAILED SYSCALL: %s -> %d (expected %s %lli), errno: %d\nat: %s:%d\n", #call, ret ## __COUNTER__, #cmp, (long long)expected, errno, __FILE__, __LINE__);\
    _SYSCALLabortUnlock();\
    _SYSCALLabort();\
  }\
  ret ## __COUNTER__; })
#endif
