/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _RuntimeImpl_h_
#define _RuntimeImpl_h_ 1

#include "runtime/Runtime.h"

#if defined(__KOS__)

#include "kernel/KernelProcessor.h"
#include "kernel/Process.h"

inline void Runtime::noStackSwitch(StackContext* currStack) {  // run tlb invalidation
  Thread* currThread = reinterpret_cast<Thread*>(currStack);
  AddressSpace& currAS = currThread->getAS();
  currAS.switchTo(currAS);
}

inline void Runtime::preStackSwitch(StackContext* currStack, StackContext* nextStack) {
  Thread* currThread = reinterpret_cast<Thread*>(currStack);
  Thread* nextThread = reinterpret_cast<Thread*>(nextStack);
  AddressSpace& currAS = currThread->getAS();
  currAS.preStackSwitch(currThread);
  currAS.switchTo(nextThread->getAS());
  LocalProcessor::setCurrStack(nextStack, _friend<Runtime>());
}

inline void Runtime::postStackSwitch(StackContext* newStack) {
  Thread* newThread = reinterpret_cast<Thread*>(newStack);
  newThread->getAS().postStackSwitch(newThread);
}

inline void Runtime::stackDestroy(StackContext* prevStack) {
  Thread* prevThread = reinterpret_cast<Thread*>(prevStack);
  if (prevThread->getAS().threadTerminated()) {
    Process* p = &reinterpret_cast<Process&>(prevThread->getAS());
    delete p;
  }
  prevThread->destroy(_friend<Runtime>());
}

extern TimerQueue* kernelTimerQueue;
inline TimerQueue& CurrTimerQueue() { return *kernelTimerQueue; }

#elif defined(__LIBFIBRE__)

#include "libfibre/Fibre.h"
#include "libfibre/EventScope.h"

inline void Runtime::noStackSwitch(StackContext* currStack) {}

inline void Runtime::preStackSwitch(StackContext* currStack, StackContext* nextStack) {
  Fibre* currFibre = reinterpret_cast<Fibre*>(currStack);
  Fibre* nextFibre = reinterpret_cast<Fibre*>(nextStack);
  currFibre->deactivate(nextFibre, _friend<Runtime>());
  Context::setCurrStack(*nextFibre, _friend<Runtime>());
}

inline void Runtime::postStackSwitch(StackContext* newStack) {
  Fibre* newFibre = reinterpret_cast<Fibre*>(newStack);
  newFibre->activate(_friend<Runtime>());
}

inline void Runtime::stackDestroy(StackContext* prevStack) {
  Fibre* prevFibre = reinterpret_cast<Fibre*>(prevStack);
  prevFibre->destroy(_friend<Runtime>());
}

inline void Runtime::notifyTimeout(const Time& t) { CurrEventScope().setTimer(t); }

inline TimerQueue& CurrTimerQueue() { return CurrEventScope().getTimerQueue(); }

#else
#error undefined platform: only __KOS__ or __LIBFIBRE__ supported at this time
#endif

#endif /* _RuntimeImpl_h_ */
